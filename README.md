# Vadim Static Site Generator

A static site generator written in Python. Obviously not as fast as Hugo, but shouldn't be a deterrent if all you want to blog is about cat memes.